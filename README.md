This repository contains a workflow how to create a hg19 GFF3 with tRNA information.

The annotation was downloaded from Gencode (v23, liftover to GRCh37) and missannotated vaultRNAs gene types were fixed (script: vaultFix.py).
tRNAscan data was downloaded from http://gtrnadb.ucsc.edu/ data base and a modified version of  https://github.com/jorvis/biocode/blob/master/gff/convert_tRNAScanSE_to_gff3.pl was used to convert it to GFF3 format.
The Snakemake workflow and the scripts are available at https://git.embl.de/schwarzl/gencode-v23-tRNA-annotation.

Author: Thomas Schwarzl <schwarzl@embl.de>