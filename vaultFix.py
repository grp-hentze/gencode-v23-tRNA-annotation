import sys

n = 0 

if __name__ == "__main__":
    for line in sys.stdin:
        n += 1

        if line.startswith("#"):
            sys.stdout.write(line)
        else:
            try:
                x = line.split()
                if len(x) > 8:
                    y = x[8].split(";")
                    
                    for i in y:
                        #sys.stdout.write("## I is: " + i + "\n")
                        if i == "gene_name=VTRNA3-1P":
                            line = line.replace("misc_RNA", "vaultRNA_pseudogene")
                            break
                        elif i == "gene_name=VTRNA2-1":
                            if x[2] == "gene":
                                line = "chr5\tHentze\tgene\t135416187\t135416286\t.\t-\t.\tID=ENSG00000270123.3;gene_id=ENSG00000270123.3;gene_type=vaultRNA;gene_status=KNOWN;gene_name=VTRNA2-1;level=2;havana_gene=OTTHUMG00000183992.1;remap_status=full_contig;remap_num_mappings=1;remap_target_status=overlap"
                            elif x[2] == "transcript":
                                line = "chr5\tHentze\ttranscript\t135416187\t135416286\t.\t-\t.\tID=ENST00000365160.1;Parent=ENSG00000270123.3;gene_id=ENSG00000270123.3;transcript_id=ENST00000365160.1;gene_type=vaultRNA;gene_status=KNOWN;gene_name=VTRNA2-1;transcript_type=vaultRNA;transcript_status=KNOWN;transcript_name=VTRNA2-1-001;level=2;transcript_support_level=NA;tag=basic;havana_gene=OTTHUMG00000183992.1;havana_transcript=OTTHUMT00000467708.1;remap_num_mappings=1;remap_status=full_contig;remap_target_status=overlap"
                            elif x[2] == "exon":
                                line = "chr5\tHentze\texon\t135416187\t135416286\t.\t-\t.\tID=exon:ENST00000365160.1:1;Parent=ENST00000365160.1;gene_id=ENSG00000270123.3;transcript_id=ENST00000365160.1;gene_type=vaultRNA;gene_status=KNOWN;gene_name=VTRNA2-1;transcript_type=vaultRNA;transcript_status=KNOWN;transcript_name=VTRNA2-1-001;exon_number=1;exon_id=ENSE00003282953.1;level=2;transcript_support_level=NA"
                            line = line + "\n"
                            break
                        elif i.startswith("gene_name=VTRNA") or i.startswith("transcript_name=VTRNA"):
                            line = line.replace("misc_RNA", "vaultRNA")
                            break
                        

                    sys.stdout.write(line)

                else:
                    sys.stdout.write(str(len(x)) + " " + str(n) + "\n")
                    sys.stdout.write(line)
                    sys.exit(1)
                
            except IndexError:
                print("Index Error at line " + str(n) + "\n '" + line + "'\n\n'" + str(len(x)) + "'")
                sys.exit(1)


