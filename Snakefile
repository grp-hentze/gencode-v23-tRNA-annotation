# Thomas Schwarzl <schwarzl@embl.de>
#
# This snakemake workflow add tRNA location info from tRNA scan to gencode v23 lift 23 gff3.
#
# Requirements: snakemake
#
# Usage: snakemake


# Inputs
GENCODE  = "../../gencode.v23lift37.annotation.gff3"
TRNASCAN = "hg19-tRNAs.out.nohap"

rule all:
	input:
		"gencode.v23lift37.annotation.plus.tRNAs.tmp.gff3"
	output:
		"gencode.v23lift37.annotation.plus.tRNAs.gff3"
	message:
		"fixing the gene type for the misslabeled vault RNAs and replacing transcript for VTRNA2-1"
	shell:
		"cat {input} | python vaultFix.py > {output}"

rule merge:
	input:
		trna    = "hg19-tRNA.gff3",
		gencode = GENCODE
	output:
		temp("gencode.v23lift37.annotation.plus.tRNAs.tmp.gff3")
	message:
		"fusing gencode gff with trna gff"
	shell:
		"cat {input.gencode} {input.trna} > {output}"

rule tRNA:
	input:
		TRNASCAN
	output:
		"hg19-tRNA.gff3"
	message:
		"converting tRNA scan output to gff3 with a modified script from https://github.com/jorvis/biocode/blob/master/gff/convert_tRNAScanSE_to_gff3.pl"
	shell:
		"perl convert_tRNAScanSE_to_gff3.pl --input {input} > {output}"


